package com.matikitli.repositories;

import com.matikitli.entities.exercisemodel.ExerciseModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExerciseModelRepo extends JpaRepository<ExerciseModel, Long> {

}
