package com.matikitli.transformers.exercise;

import com.matikitli.entities.exercise.Exercise;
import com.matikitli.entities.exercisemodel.ExerciseModel;
import com.matikitli.models.exercise.ExerciseDTO;
import com.matikitli.models.exercisemodel.ExerciseModelDTO;
import com.matikitli.services.exercisemodel.ExerciseModelService;
import com.matikitli.transformers.exercisemodel.ExerciseModelTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Component
public class ExerciseTransformer {

    @Autowired
    private ExerciseModelTransformer exerciseModelTransformer;

    @Autowired
    private ExerciseModelService exerciseModelService;

    public Exercise fromDtoToEntity(ExerciseDTO source, Exercise target) {
        ExerciseModel model = fetchModel(source.getExerciseModel(), target.getExerciseModel());
        target.setExerciseModel(model);

        if (source.getDate() != null) {
            target.setDate(source.getDate());
        } else {
            target.setDate(LocalDate.now());
        }

        target.setReps(source.getReps());
        target.setMass(source.getMass());
        target.setWorkoutId(source.getWorkoutId());
        return target;
    }

    public ExerciseDTO fromEntityToDto(Exercise source, ExerciseDTO target) {

        return target;
    }

    private ExerciseModel fetchModel(ExerciseModelDTO sourceDto, ExerciseModel targetDto) {
        ExerciseModel result = targetDto;
        //exerciseModelService
        //exerciseModelTransformer
        return result;
    }
}
