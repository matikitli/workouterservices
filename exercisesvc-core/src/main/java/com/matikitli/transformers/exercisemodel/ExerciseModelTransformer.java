package com.matikitli.transformers.exercisemodel;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.matikitli.entities.exercisemodel.ExerciseModel;
import com.matikitli.entities.exercisemodel.ExerciseModelTarget;
import com.matikitli.entities.exercisemodel.ExerciseModelType;
import com.matikitli.models.exercisemodel.ExerciseModelDTO;
import com.matikitli.models.exercisemodel.ExerciseModelTargetDTO;
import com.matikitli.models.exercisemodel.ExerciseModelTypeDTO;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ExerciseModelTransformer {

    private static final BiMap<ExerciseModelTypeDTO, ExerciseModelType> EXERCISE_MODEL_TYPE_DTO_TO_ENTITY;
    private static final BiMap<ExerciseModelTargetDTO, ExerciseModelTarget> EXERCISE_MODEL_TARGET_DTO_TO_ENTITY;
    //populate EXERCISE_MODEL_TYPE_DTO_TO_ENTITY
    static {
        BiMap<ExerciseModelTypeDTO, ExerciseModelType> exerciseModelTypeDtoToEntity = HashBiMap.create();
        exerciseModelTypeDtoToEntity.put(ExerciseModelTypeDTO.FBW, ExerciseModelType.FBW);
        exerciseModelTypeDtoToEntity.put(ExerciseModelTypeDTO.LIFT, ExerciseModelType.LIFT);
        exerciseModelTypeDtoToEntity.put(ExerciseModelTypeDTO.SW, ExerciseModelType.SW);
        EXERCISE_MODEL_TYPE_DTO_TO_ENTITY = exerciseModelTypeDtoToEntity;
    }
    //populate EXERCISE_MODEL_TARGET_DTO_TO_ENTITY
    static {
        BiMap<ExerciseModelTargetDTO, ExerciseModelTarget> exerciseModelTargetDtoToEntity = HashBiMap.create();
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.BICEPS, ExerciseModelTarget.BICEPS);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.TRICEPS, ExerciseModelTarget.TRICEPS);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.FOREARMS  , ExerciseModelTarget.FOREARMS);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.SHOULDERS_FRONT, ExerciseModelTarget.SHOULDERS_FRONT);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.SHOULDERS_MIDDLE, ExerciseModelTarget.SHOULDERS_MIDDLE);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.SHOULDERS_BACK, ExerciseModelTarget.SHOULDERS_BACK);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.CORE_UP, ExerciseModelTarget.CORE_UP);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.CORE_MIDDLE, ExerciseModelTarget.CORE_MIDDLE);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.CORE_DOWN, ExerciseModelTarget.CORE_DOWN);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.BACK_UP, ExerciseModelTarget.BACK_UP);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.BACK_LOWER, ExerciseModelTarget.BACK_LOWER);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.BACK_WINGS, ExerciseModelTarget.BACK_WINGS);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.ABS_MIDDLE, ExerciseModelTarget.ABS_MIDDLE);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.ABS_UP, ExerciseModelTarget.ABS_UP);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.ABS_DOWN, ExerciseModelTarget.ABS_DOWN);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.ABS_SITE, ExerciseModelTarget.ABS_SITE);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.LEGS_FRONT, ExerciseModelTarget.LEGS_FRONT);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.LEGS_BACK, ExerciseModelTarget.LEGS_BACK);
        exerciseModelTargetDtoToEntity.put(ExerciseModelTargetDTO.LEGS_CALVS, ExerciseModelTarget.LEGS_CALVS);
        EXERCISE_MODEL_TARGET_DTO_TO_ENTITY = exerciseModelTargetDtoToEntity;
    }

    public ExerciseModel fromDtoToEntity(ExerciseModelDTO source, ExerciseModel target) {

        target.setModelName(source.getName());
        target.setModelType(EXERCISE_MODEL_TYPE_DTO_TO_ENTITY.get(source.getType()));
        target.setModelTarget(source.getTargets()
                .stream().map(EXERCISE_MODEL_TARGET_DTO_TO_ENTITY::get).collect(Collectors.toSet()));
        return target;
    }

    public ExerciseModelDTO fromEntityToDto(ExerciseModel source, ExerciseModelDTO target) {

        return target;
    }
}
