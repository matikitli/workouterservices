package com.matikitli.entities.exercise;

import com.matikitli.entities.exercisemodel.ExerciseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "exercise")
        //, uniqueConstraints = {@UniqueConstraint( columnNames = {""})})
public class Exercise {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long exerciseId;

    @Column(name = "workout_id")
    private Long workoutId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "model_id", nullable = false)
    private ExerciseModel exerciseModel;

    @Column(name = "reps")
    private Double reps;

    @Column(name = "mass")
    private Double mass;

    @Column(name = "date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDate date;
}
