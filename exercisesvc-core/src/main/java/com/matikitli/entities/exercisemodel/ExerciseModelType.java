package com.matikitli.entities.exercisemodel;

public enum ExerciseModelType {
    FBW,
    SW,
    LIFT
}
