package com.matikitli.entities.exercisemodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "exercise_model")
        //, uniqueConstraints = {@UniqueConstraint( columnNames = {"name"}) })
public class ExerciseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long modelId;

    @ElementCollection(targetClass = ExerciseModelTarget.class)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "exercise_model_target")
    @Column(name = "target")
    private Set<ExerciseModelTarget> modelTarget;

    @Column(name = "name", unique = true)
    private String modelName;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private ExerciseModelType modelType;


}
