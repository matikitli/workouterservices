package com.matikitli.models.workout;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.LocalDate;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorkoutDTO {

    private Long id;

    private LocalDate date;

    private Duration duration;

    private Set<WorkoutFocusDTO> focus;

    private Integer power;

}
