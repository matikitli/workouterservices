package com.matikitli.models.workout;

public enum WorkoutFocusDTO {
    ARMS,
    SHOULDERS,
    CORE,
    BACK,
    ABS,
    LEGS;
}
