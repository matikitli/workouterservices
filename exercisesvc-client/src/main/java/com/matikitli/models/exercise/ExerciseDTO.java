package com.matikitli.models.exercise;

import com.matikitli.models.exercisemodel.ExerciseModelDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExerciseDTO {

    private Long id;

    private Long workoutId;

    private ExerciseModelDTO exerciseModel;

    private Double reps;

    private Double mass;

    private LocalDate date;

}
