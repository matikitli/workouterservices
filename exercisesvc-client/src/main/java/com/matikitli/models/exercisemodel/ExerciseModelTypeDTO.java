package com.matikitli.models.exercisemodel;

public enum ExerciseModelTypeDTO {
    FBW,
    SW,
    LIFT
}
