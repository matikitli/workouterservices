package com.matikitli.models.exercisemodel;

public enum ExerciseModelTargetDTO {
    BICEPS,
    TRICEPS,
    FOREARMS,

    SHOULDERS_FRONT,
    SHOULDERS_MIDDLE,
    SHOULDERS_BACK,

    CORE_UP,
    CORE_MIDDLE,
    CORE_DOWN,

    BACK_UP,
    BACK_LOWER,
    BACK_WINGS,

    ABS_MIDDLE,
    ABS_UP,
    ABS_DOWN,
    ABS_SITE,

    LEGS_FRONT,
    LEGS_BACK,
    LEGS_CALVS;
}
