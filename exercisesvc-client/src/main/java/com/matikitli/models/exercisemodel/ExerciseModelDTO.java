package com.matikitli.models.exercisemodel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExerciseModelDTO {

    private Long id;

    private String name;

    private Set<ExerciseModelTargetDTO> targets;

    private ExerciseModelTypeDTO type;

}
