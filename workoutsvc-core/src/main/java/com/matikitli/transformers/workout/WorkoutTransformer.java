package com.matikitli.transformers.workout;

import com.matikitli.entities.workout.Workout;
import com.matikitli.models.workout.WorkoutDTO;
import org.springframework.stereotype.Component;

@Component
public class WorkoutTransformer {

    public Workout fromDtoToEntity(WorkoutDTO source, Workout target) {

        return target;
    }

    public WorkoutDTO fromEntityToDto(Workout source, WorkoutDTO target) {

        return target;
    }
}
