package com.matikitli.entities.workout;

public enum WorkoutFocus {
    ARMS,
    SHOULDERS,
    CORE,
    BACK,
    ABS,
    LEGS;
}
