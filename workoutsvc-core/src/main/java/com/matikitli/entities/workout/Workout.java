package com.matikitli.entities.workout;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.convert.DurationFormat;
import org.springframework.boot.convert.DurationStyle;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "workout")
                //, uniqueConstraints = {@UniqueConstraint( columnNames = {""})})
public class Workout {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long workoutId;

    @Column(name = "date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDate date;

    @Column(name = "duration", nullable = true)
    @DurationFormat(value = DurationStyle.SIMPLE)
    private Duration duration;

    @ElementCollection(targetClass = WorkoutFocus.class)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "workout_focus")
    @Column(name = "focus", nullable = true)
    private Set<WorkoutFocus> focus;

    @Column(name = "power", nullable = true)
    private Integer power;
}
