package com.matikitli.repositories;

import com.matikitli.entities.workout.Workout;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkoutRepo extends JpaRepository<Workout, Long> {

}
